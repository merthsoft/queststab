﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Merthsoft.QuestTab.Compatability {
    public static class Standardized {
        public const string Label = "QuestTab_Label";
        public const string TicksLeft = "QuestTab_TicksLeft";
        public const string Hostility = "QuestTab_Hostility";
        public const string IsQuest = "QuestTab_IsQuest";
        public const string IsBase = "QuestTab_IsBase";
        public const string ShouldHide = "QuestTab_ShouldHide";
        public const string InspectString = "QuestTab_InspectString";
    }
}
