﻿using RimWorld;
using RimWorld.Planet;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Verse;

namespace Merthsoft.QuestTab.MainTabWindow {
    public class QuestTabWindow : RimWorld.MainTabWindow {
        public enum QuestTabType {
            Quests,
            Bases,
            AllWorldObjects,
        };
        
        public enum SortMode {
            Alphabetical,
            ExpirationTime,
            Hostility
        };
        
        public enum SortDirection {
            Ascending,
            Descending
        };

        public static Dictionary<QuestTabType, HashSet<SortMode>> SupportedSortModes = new Dictionary<QuestTabType, HashSet<SortMode>> {
            { QuestTabType.Quests, new HashSet<SortMode> { SortMode.Alphabetical, SortMode.ExpirationTime, } },
            { QuestTabType.Bases, new HashSet<SortMode> { SortMode.Alphabetical, SortMode.Hostility, } },
            { QuestTabType.AllWorldObjects, new HashSet<SortMode> { SortMode.Alphabetical, } },
        };
        
        private float scrollViewHeight = 0f;
        private Vector2 scrollPosition = Vector2.zero;

        private QuestTabType tabType = QuestTabType.Quests;
        private SortMode sort = SortMode.Alphabetical;
        public SortDirection Direction = SortDirection.Ascending;

        public override Vector2 RequestedTabSize => new Vector2(500, 684);

        public QuestTabType TabType {
            get => tabType;
            set {
                tabType = value;
                Sort = SortMode.Alphabetical;
            }
        }

        public SortMode Sort {
            get => sort;
            set {
                sort = value;
                Direction = SortDirection.Ascending;
            }
        }

        public override void DoWindowContents(Rect fillRect) {
            base.DoWindowContents(fillRect);

            var position = new Rect(0f, 0f, fillRect.width, fillRect.height);

            GUI.BeginGroup(position);
            Text.Font = GameFont.Medium;
            GUI.color = Color.white;
            var outRect = new Rect(0f, 50f, position.width, position.height - 50f);
            var rect = new Rect(0f, 0f, position.width - 16f, scrollViewHeight);

            var cursor = Dropdown(0, 0, GetQuestTypeDropdownMenuElements, TabType.GetLabel());
            cursor = Dropdown(cursor.x, cursor.y, GetSortModeDropdownMenuElements, Sort.GetLabel());
            Dropdown(cursor.x, cursor.y, GetSortDirectionDropDownMenuElements, Direction.GetLabel());

            Widgets.BeginScrollView(outRect, ref scrollPosition, rect, true);
            float rowY = 0f;

            IEnumerable<WorldObject> objects = Find.World.worldObjects.FilterByQuestTabType(TabType).OrderBy((wo) => wo.GetSortKey(Sort));
            
            if (Direction == SortDirection.Descending) {
                objects = objects.Reverse();
            }

            foreach (var worldObject in objects) {
                GUI.color = new Color(1f, 1f, 1f, 0.2f);
                Widgets.DrawLineHorizontal(0f, rowY, rect.width);
                GUI.color = Color.white;
                DrawSiteRow(worldObject, ref rowY, rect);
            }

            scrollViewHeight = rowY;

            Widgets.EndScrollView();
            GUI.EndGroup();
        }

        private Vector2 Dropdown<TPayload>(float x, float y, Func<QuestTabWindow, IEnumerable<Widgets.DropdownMenuElement<TPayload>>> menuGenerator, string buttonLabel) {
            var textSize = Text.CalcSize(buttonLabel);
            Widgets.Dropdown(new Rect(x, y, textSize.x + 20, textSize.y), this, null, menuGenerator, buttonLabel);
            return new Vector2(x + textSize.x + 20, y);
        }

        private IEnumerable<Widgets.DropdownMenuElement<QuestTabType>> GetQuestTypeDropdownMenuElements(QuestTabWindow tabWindow)
            => ((QuestTabType[])Enum.GetValues(typeof(QuestTabType)))
                .Select((q) => q.ToDropDownMenuElement(tabWindow));

        public bool TabTypeCanSupportSortMode(SortMode s)
            => SupportedSortModes[TabType].Contains(s);

        private IEnumerable<Widgets.DropdownMenuElement<SortMode>> GetSortModeDropdownMenuElements(QuestTabWindow tabWindow)
            => ((SortMode[])Enum.GetValues(typeof(SortMode)))
                .Where((s) => tabWindow.TabTypeCanSupportSortMode(s))
                .Select((s) => s.ToDropDownMenuElement(tabWindow));

        private IEnumerable<Widgets.DropdownMenuElement<SortDirection>> GetSortDirectionDropDownMenuElements(QuestTabWindow tabWindow)
            => ((SortDirection[])Enum.GetValues(typeof(SortDirection)))
                .Select((s) => s.ToDropDownMenuElement(tabWindow));

        protected void DrawSiteRow(WorldObject worldObject, ref float rowY, Rect fillRect) {
            var width = fillRect.width - 10;
            
            Text.Font = GameFont.Medium;
            Text.Anchor = TextAnchor.UpperLeft;

            var text = worldObject.LabelCap;
            if (TabType == QuestTabType.Quests) {
                text = worldObject.GetQuestLabel();
            }
            rowY += DrawAndMeasureString(text, new Rect(35f, rowY, width, InspectPaneUtility.PaneHeight));

            Text.Font = GameFont.Small;
            Text.Anchor = TextAnchor.UpperLeft;
            rowY += DrawAndMeasureString(worldObject.GetQuestInspectString(), new Rect(35f, rowY, width, InspectPaneUtility.PaneHeight));

            rowY += DrawJumpLocationLabel(worldObject, new Rect(35f, rowY, width, InspectPaneUtility.PaneHeight));
        }

        private float DrawAndMeasureString(string s, Rect rect) {
            Widgets.Label(rect, s);
            return Text.CalcHeight(s, rect.width);
        }

        protected float DrawJumpLocationLabel(WorldObject worldObject, Rect rect) {
            DiaOption diaOption = new DiaOption("JumpToLocation".Translate());
            diaOption.action = delegate {
                Close();
                CameraJumper.TryJumpAndSelect(new GlobalTargetInfo(worldObject));
            };

            return diaOption.OptOnGUI(rect);
        }
    }
}
