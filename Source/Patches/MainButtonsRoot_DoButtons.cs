﻿using Harmony;
using RimWorld;
using RimWorld.Planet;
using System.Linq;
using Verse;

namespace Merthsoft.QuestTab.Patches {
    [HarmonyPatch(typeof(MainButtonsRoot), "DoButtons")]
    public static class MainButtonsRoot_DoButtons {
        public static void Prefix() {
            var visible = QuestTabMod.Settings.HideTabWhenNotOnWorldMenu ? Find.World.renderer.wantedMode == WorldRenderMode.Planet : true;
            DefDatabase<MainButtonDef>.AllDefs.First(d => d.defName == "Merthsoft_QuestTab_MainButtonDef_Quests").buttonVisible = visible;
        }
    }
}
