﻿using Verse;

namespace Merthsoft.QuestTab {
    public class QuestTabModSettings : ModSettings {
        public bool HideTabWhenNotOnWorldMenu = false;

        public QuestTabModSettings() {
        }

        public override void ExposeData() {
            base.ExposeData();

            Scribe_Values.Look(ref HideTabWhenNotOnWorldMenu, "HideTabWhenNotOnWorldMenu", false);
        }
    }
}
