﻿using RimWorld.Planet;
using Verse;

namespace Merthsoft.QuestTab {
    public static class WorldObjectCompExtensions {
        public static bool MfiBumperCropComp_ActiveRequest(this WorldObjectComp settlementBumperCropComp)
            => (bool)settlementBumperCropComp.GetInstanceProperty("ActiveRequest");

        public static int MfiBumperCropComp_Expiration(this WorldObjectComp settlementBumperCropComp)
            => (int)settlementBumperCropComp.GetInstanceField("expiration");

        public static int MfiBumperCropComp_TicksLeft(this WorldObjectComp settlementBumperCropComp)
            => settlementBumperCropComp.MfiBumperCropComp_Expiration() - Find.TickManager.TicksGame;

        public static int TicksLeft(this TradeRequestComp tradeRequestComp)
            => tradeRequestComp.expiration - Find.TickManager.TicksGame;
    }
}
