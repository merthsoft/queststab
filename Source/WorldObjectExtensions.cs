﻿using Merthsoft.QuestTab.Compatability;
using RimWorld.Planet;
using System;
using System.Collections.Generic;
using System.Linq;
using Verse;
using static Merthsoft.QuestTab.MainTabWindow.QuestTabWindow;

namespace Merthsoft.QuestTab {
    public static class WorldObjectExtensions {
        public static object GetSortKey(this WorldObject worldObject, SortMode mode) {
            switch (mode) {
                case SortMode.Alphabetical:
                    return worldObject.GetQuestLabel();
                case SortMode.ExpirationTime:
                    if (worldObject.InstanceHasProperty<int>(Standardized.TicksLeft, out var ticksLeft)) { return ticksLeft; }

                    var timeoutComp = worldObject.GetComponent<TimeoutComp>();
                    if (timeoutComp != null) { return timeoutComp.TicksLeft; }

                    var tradeRequestComp = worldObject.GetComponent<TradeRequestComp>();
                    if (tradeRequestComp != null) { return tradeRequestComp.TicksLeft(); }

                    var settlementBumperCropComp = worldObject.GetMfiBumperCropComp();
                    if (settlementBumperCropComp != null) { return settlementBumperCropComp.MfiBumperCropComp_TicksLeft(); }

                    return -1;
                case SortMode.Hostility:
                    return worldObject.GetInstancePropertyWithFallback(Standardized.Hostility,
                        worldObject.Faction == null ? -1000 : worldObject.Faction == Find.FactionManager.OfPlayer ? 1000 : worldObject.Faction.GoodwillWith(Find.FactionManager.OfPlayer));
                default:
                    throw new Exception($"Unknown soft mode {mode}.");
            }
        }

        public static IEnumerable<WorldObject> FilterByQuestTabType(this WorldObjectsHolder holder, QuestTabType questTabType) {
            var objects = holder.AllWorldObjects;
            switch (questTabType) {
                case QuestTabType.Quests:
                    return objects.Where(IsQuest);
                case QuestTabType.Bases:
                    return objects.Where(IsBase);
                case QuestTabType.AllWorldObjects:
                    return objects.Where(IsDisplayableWorldObject); 
                default:
                    throw new Exception($"Unknown tab type {questTabType}.");
            }
        }

        public static bool IsDisplayableWorldObject(this WorldObject worldObject)
            => !worldObject.HideFromQuestTab() && !(worldObject is Caravan) && !string.IsNullOrEmpty(worldObject.LabelCap);

        public static WorldObjectComp GetMfiBumperCropComp(this WorldObject worldObject)
            => MoreFactionInteraction.SettlementBumperCropComp == null ? null : worldObject.GetComponent(MoreFactionInteraction.SettlementBumperCropComp);

        public static bool IsBase(this WorldObject worldObject) {
            if (worldObject.HideFromQuestTab()) { return false; }
            if (worldObject.InstanceHasProperty<bool>(Standardized.IsBase, out var isBase)) { return isBase; }

            return typeof(SettlementBase).IsAssignableFrom(worldObject.GetType());
        }

        public static bool HideFromQuestTab(this WorldObject worldObject)
            => worldObject.GetInstancePropertyWithFallback(Standardized.ShouldHide, false);

        public static bool IsQuest(this WorldObject worldObject) {
            if (worldObject.HideFromQuestTab()) { return false; }
            if (worldObject.InstanceHasProperty<bool>(Standardized.IsQuest, out var isQuest)) { return isQuest; }

            if (worldObject.GetComponent<TimeoutComp>() != null) { return worldObject.def.defName != SetUpCamp.AbandonedCampDefName; }
            if (worldObject.GetComponent<TradeRequestComp>()?.ActiveRequest ?? false) { return true; }
            if (worldObject.GetMfiBumperCropComp()?.MfiBumperCropComp_ActiveRequest() ?? false) { return true; }

            return false;
        }

        public static string GetQuestLabel(this WorldObject worldObject) {
            if (worldObject.InstanceHasProperty<string>(Standardized.Label, out var questLabel)) { return questLabel; }

            if (worldObject.GetComponent<TradeRequestComp>()?.ActiveRequest ?? false) { return "LetterLabelCaravanRequest".Translate(); }
            if (worldObject.GetMfiBumperCropComp()?.MfiBumperCropComp_ActiveRequest() ?? false) { return "MFI_LetterLabel_HarvestRequest".Translate(); }

            return worldObject.LabelCap;
        }

        public static string GetQuestInspectString(this WorldObject worldObject) {
            if (worldObject.InstanceHasProperty<string>(Standardized.InspectString, out var inspectString)) { return inspectString; }

            return worldObject.GetInspectString();
        }
    }
}
